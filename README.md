newsbot-lite
===================
IRC bot to display top news stories


#Deprecated
**newsbot** has now merged into the `hn` submodule of the [CasulBot IRC Python Engine](https://bitbucket.org/mitch-b/casul-ircbot-python). 





##Dependencies
**newsbot** uses the [CasulBot IRC Python Engine](https://bitbucket.org/mitch-b/casul-ircbot-python) for connections/communication. This engine has been included as a submodule to this repository.

##Getting Started
In order to get up and running, you'll need [Python 2.7](https://www.python.org/download/releases/2.7.6/). In your terminal, navigate to where you want to clone this project. Run the following commands to pull all required files:

```bash
$> git clone https://bitbucket.org/abraxas-dev/newsbot-lite.git
$> cd newsbot-lite
$> git submodule update --init --recursive
$> pip install requests
```

If you do not have `pip` installed, you will probably want to do so. It's fantastic.[pip Installation Guide](https://pip.pypa.io/en/latest/installing.html).

This will clone the repository, and pull latest of the submodule (casul IRC Python Engine) with it. This will allow you to make any changes to the engine (that you don't want back in actual engine itself), while still being able to pull latest updates to code base (if wanted).

##Configuring

You'll want to create a new file named `config.ini` in the root directory of newsbot-lite.
The contents of the file should look like this, simply modify the values as needed:

```bash
[casulirc]
About: News Bot
	> mitch-b
	> https://bitbucket.org/abraxas-dev/newsbot-lite
	0.0.2, 06/04/2014
Verbose: True
Host: irc.server.com
Port: 6667
Channel: #channel
Password:
Nick: newsbot
User: newsbot
HostName: server.com
ServerName: server
RealName: NewsBot
```

This file is found in the .gitignore, so you won't need to worry about accidentally commiting any sensitive information.

##Testing

Simply run:

```bash
$> python newsbot.py
```

and the casul IRC Engine will attempt to connect and listen to the specified channel. You will want to join the same channel on the IRC server and try communicating with your bot!

```
> [your_nick]: hi !newsbot ...
>   [newsbot]: Hello, your_nick!
```