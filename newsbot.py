#!/usr/bin/env python

# The MIT License (MIT)
#
# Copyright (c) 2014 Mitchell Barry
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

__author__ = "Mitchell Barry"
__email__ = "mitch.barry@gmail.com"

"""IRC News Bot

Display top news story from included news sources
"""

from engine.casulirc import engine
from engine.casulirc import settings
import ConfigParser
import requests
import random
import json

greetings = [
  "Hi, {0}!",
  "Yo, {0}!",
  "{0}, my friend ...",
  "Sorry to keep you waiting, {0}."
]

class NewsBot:
  def __init__(self):
    global bot
    newsSettings = settings.Settings()

    config = ConfigParser.ConfigParser()
    config.read('./config.ini')
    newsSettings.about = config.get('casulirc', 'About')
    newsSettings.host = config.get('casulirc', 'Host')
    newsSettings.channel = config.get('casulirc', 'Channel')
    newsSettings.password = config.get('casulirc', 'Password')
    newsSettings.nick = config.get('casulirc', 'Nick')
    newsSettings.verbose = config.get('casulirc', 'Verbose')
    newsSettings.user = config.get('casulirc', 'User')
    newsSettings.hostname = config.get('casulirc', 'HostName')
    newsSettings.server = config.get('casulirc', 'ServerName')
    newsSettings.realname = config.get('casulirc', 'RealName')

    bot = engine.CasulIRC(callback=self.callback,
                          settings=newsSettings)
    bot.run()

  def random_greeting(self, name):
    return random.choice(greetings).format(name)

  def callback(self, sender, message):
    message = self.random_greeting(sender)
    bot.PRIVMSG("{0} Let me grab the latest data for you...".format(message))
    url = "http://hnify.herokuapp.com/get/top?limit=1"
    try:
      result = requests.get(url)
      if result.status_code == 200:
        print 'Parsing JSON...'
        data = json.loads(result.text)
        story = data['stories'][0]
        title = story['title']
        link = story['link']
      else:
        print 'Unable to read from {0}'.format(url)
    except Exception as e:
      print e
    if title and link:
      # TODO: fix encoding
      article_title = title.encode('utf-8', 'replace')
      response = '{0} - read about {1} : {2}'.format(sender, article_title, link)
      bot.PRIVMSG(response)
    else:
      bot.PRIVMSG("Hello, {0}! Sorry, I can't seem to function right today".format(sender))

if __name__ == '__main__':
  NewsBot()
